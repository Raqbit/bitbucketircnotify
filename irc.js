const hooks = require('./hooks');
const ircfw = require('irc-framework');

let bots = {};

const newBot = (network, config) => {
    const log = require('./logger')('IRC/' + network);

    const bot = new ircfw.Client({
        gecos: config.user + ' - Bitbucket IRC Notification Bot',
        host: config.host,
        nick: config.user,
        password: config.password,
        port: config.port,
        ssl: config.ssl,
        username: config.user,
    });

    bot.on('raw', (event) => {
        log.debug(`[${network}] [S ${event.from_server ? '➡' : '⬅'} B] ${event.line}`);
    });

    bot.on('nick in use', () => {
        bot.changeNick(bot.user.nick + '_');
        log.info('Nick already in use - changed to ' + bot.user.nick);
    });

    bot.registered = false;
    bot.registered_queue = []; // Each element: [method to call, arguments in an array]
    bot.on('registered', () => {
        log.info('Connected!');
        bot.registered = true;
        for (let [method, args] of bot.registered_queue) {
            method.apply(null, args || []);
        }
    });

    // Keep track of which channels we joined
    bot.channels = [];
    bot.on('join', (event) => {
        if (event.nick === bot.user.nick) {
            log.info('Joined ' + event.channel);
            bot.channels.push(event.channel.toLowerCase());
        }
    });

    bot.joinSay = (channel, message) => {
        channel = channel.toLowerCase();
        if (!bot.registered) {
            bot.registered_queue.push([bot.joinSay, [channel, message]]);
            log.debug('[' + network + '] Queued a message to ' + channel + ' because I\'m not properly connected yet');
            return;
        }

        // If we are in that channel, say it immediately
        if (bot.channels.indexOf(channel) !== -1) {
            bot.say(channel, message);
        }
        else {
            // If not, join that channel
            bot.join(channel);

            // And listen for when we do
            let onJoin = (event) => {
                if (event.nick === bot.user.nick && event.channel.toLowerCase() === channel) {
                    // Once we joined that channel, say the message we wanted to
                    bot.say(channel, message);

                    // And stop listening for join events
                    bot.removeListener('join', onJoin);
                }
            };
            bot.on('join', onJoin);
        }
    };

    return bot;
};

const notify = (hook, messages) => {
    // Convert messages into an array if it is a string
    if (typeof messages === 'string') {
        messages = [messages];
    }

    // Get network name, channels, and network config from data files
    let {network, channels} = hooks.hooks[hook];
    let networkConfig = hooks.networks[network];

    // If there is no such bot, or it isn't connected, create it and connect
    if (!bots[network] || !bots[network].connected) {
        bots[network] = newBot(network, networkConfig);
        bots[network].connect();
    }

    for (let channel of channels.split(',')) {
        for (let message of messages) {
            // Say the message in each channel, joining it before we do
            bots[network].joinSay(channel, message);
        }
    }
};

module.exports = {
    notify,
};
