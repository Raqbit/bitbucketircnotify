const config = require('./config');
const winston = require('winston');
const colors = require('colors/safe');

module.exports = createLogger;

const logColours = {
    'info': 'cyan',
    'debug': 'gray',
    'warn': 'blue',
    'error': 'red',
};

let levelLen = Math.max(...Object.keys(logColours).map(v => v.length));

// Helpers
const padLeft = (str, amount) => ' '.repeat(Math.max(0, amount - str.length)) + str;
const zeroPad = num => num.toString().length === 1 ? `0${num}` : `${num}`;

function createLogFormatter (prefix) {
    if (config.json_logs) {
        return function formatter (options) {
            options.prefix = prefix;
            options.time = new Date().getTime();

            return JSON.stringify(options);
        };
    }

    return function formatter (options) {
        // Return string will be passed to logger. 
        const levelColor = colors[logColours[options.level.toLowerCase()]] || colors.black;
        let line = '';

        // Time
        const now = new Date();
        line += colors.gray([now.getHours(), now.getMinutes(), now.getSeconds()].map(zeroPad).join(':'));

        // Prefix
        line += ' ';
        line += colors.white('[') + colors.magenta(prefix) + colors.white(']');

        // Level
        line += ' ';
        line += colors.white('[') + levelColor(padLeft(options.level.toUpperCase(), levelLen)) + colors.white(']');

        // Message
        line += ' ';
        line += options.message ? options.message : '';

        // Meta
        line += ' ';
        line += (options.meta && Object.keys(options.meta).length) ? JSON.stringify(options.meta) : '';

        return options.colorize ? line : colors.stripColors(line);
    };
}

function createLogger (prefix) {
    return new winston.Logger({
        transports: [
            new winston.transports.Console({
                level: process.env.LOG_LEVEL ? process.env.LOG_LEVEL : (process.env.NODE_ENV === 'development' ? 'debug' : 'info'),
                colorize: process.env.LOG_COLORS ? process.env.LOG_COLORS.toLowerCase() === 'true' : true,
                formatter: createLogFormatter(prefix),
            }),
        ],
    });
}
