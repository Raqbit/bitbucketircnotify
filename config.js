// Edit the following config option

const config = {
    // The port the web server listens on
    bind_port: 4810,
    colors: true,
    json_logs: false,
};

// Stop editing here

const _ = require('lodash');
const finalConfig = _.defaultsDeep(
    // Use enviroment values
    {
        bind_port: process.env.PORT,
        colors: process.env.COLORS,
        json_logs: process.env.JSON_LOGS ? process.env.JSON_LOGS.toLowerCase() === 'true' : undefined,
    },
    // Then config values
    config
);

module.exports = finalConfig;
