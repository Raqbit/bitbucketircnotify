/*
message keys are the `eventKey`.replace(':', '_')

message objects:
    [id] string: used to identify the message  (DEFAULT: undefined)
    [message] string: the message to display  (DEFAULT: '')
    [condition] function(event) -> boolean: whether or not to display this message  (DEFAULT: true)
    [run_on] function(event) -> array[object]: what objects to test on/to display for. `event` is merged 
                                               with each object returned before formatting and testing
                                               condition (DEFAULT: [event])
    [sub_messages] array[string]: Message objects (ids) to evaluate after this succeeds. It's event object 
                                  will have `sub_message: True` key/value. It will be evaluated for each
                                  object returned in `run_on`. The format for the strings are: `event_key.messageid`, eg. 'repo_push.commit'

Only the first message whose `condition` method returns true, will be displayed (excluding `sub_messages`)

Template matches `{key}` where `key` is a key of the data object (event, but sometimes another object has to be merged with it first)
The key `{key.key2.key3}` refferes to the key3 value inside key2 object inside key object
if the key is not found, it will remain in the string

SPECIAL PARSERS:
    some stuff like change parsing needs to be done
    `{parse_changes|changes}` used the parser `parse_changes` on object `changes`

    The `parse_changes` parser will check each key inside the `changes` object if they have a 'old' and 'new' key
    All those that do will be concatenated into a string like so:
        `status: 'open' -> 'closed', title: 'test' -> 'my test'`
    where 'status' and 'title' were changed
*/

module.exports = {
    /*
    * REPOSITORY MESSAGES
    */
    repo_push: [
        {
            id: 'created_branch',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} created branch {color|magenta}{branch}{color|reset} at {color|green}{last_commit.short_hash}{color|reset} - {diff}',
            condition: (event) => event.branch_created,
            run_on: (event) => event.changes,
        },
        {
            id: 'deleted_branch',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} deleted branch {color|magenta}{repository.name}/{branch}{color|reset} at {color|green}{last_commit.short_hash}{color|reset}',
            condition: (event) => event.branch_deleted,
            run_on: (event) => event.changes,
        },
        {
            id: 'tagged',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} tagged {color|orange}{tag}{color|reset} at {color|green}{last_commit.short_hash}{color|reset}',
            condition: (event) => !!event.tag,
            run_on: (event) => event.changes,
        },
        {
            id: 'force_push',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} {color|red}force pushed{color|reset} {color|magenta}{repository.name}/{branch}{color|reset} from {color|green}{from_short_hash}{color|reset} to {color|green}{to_short_hash}{color|reset} - {shorten_link|to_hash_link}',
            condition: (event) => !!event.commits && event.force_push,
            run_on: (event) => event.changes,
        },
        {
            id: 'push',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} pushed {color|bold}{commit_count}{color|reset} commits to {color|magenta}{repository.name}/{branch}{color|reset}',
            condition: (event) => !!event.commits && !event.truncated,
            run_on: (event) => event.changes,
            sub_messages: ['repo_push.commit'],
        },
        {
            id: 'push_truncated',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} pushed {color|bold}{commit_count}+{color|reset} commits to {color|magenta}{repository.name}/{branch}{color|reset}',
            condition: (event) => !!event.commits && event.truncated,
            run_on: (event) => event.changes,
            sub_messages: ['repo_push.commit'],
        },
        {
            id: 'commit',
            message: '[{color|blue}{repository.name}{color|reset}] {color|green}{short_hash}...{color|reset} {color|pink}{no_ping|author.username}{color|reset} - {message} - {shorten_link|link}',
            condition: (event) => event.sub_message,
            run_on: (event) => event.commits.slice().reverse(), // Clone array, then reverse it (Array.prototype.reverse mutates the array)
        },
    ],
    repo_fork: [
        {
            id: 'fork',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} forked {color|magenta}{repository.full_name}{color|reset} to {color|magenta}{fork.full_name}{color|reset} ({shorten_link|fork.link})',
        }
    ],
    repo_updated: [
        {
            id: 'updated',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} changed the repository {parse_changes|changes}',
        }
    ],
    repo_transfer: [
        {
            id: 'transfer',
            message: '[{color|blue}{repository.name}{color|reset}] {color|orange}{no_ping|prev_owner.username}{color|reset} transfered ownership of {color|magenta}{repository.full_name}{color|reset} to {color|pink}{no_ping|new_owner.username}{color|reset}',
        }
    ],
    repo_commit_comment_created: [
        {
            id: 'commit_comment_created',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} commented on commit {color|green}{short_hash}{color|reset}',
        }
    ],
    repo_commit_status_created: [
        {
            id: 'commit_status_created',
            message: '[{repositry.name}] {color|bold}Build created:{color|reset} {color|lblue}{commit_status.name}{color|reset} - {commit_status.description} ({commit_status.state}) - {shorten_link|commit_status.link}',
        }
    ],
    repo_commit_status_updated: [
        {
            id: 'commit_status_updated',
            message: '[{repositry.name}] {color|bold}Build updated:{color|reset} {color|blue}{commit_status.name}{color|reset} - {commit_status.description} ({commit_status.state}) - {shorten_link|commit_status.link}',
        }
    ],

    /*
    * ISSUE MESSAGES
    */
    issue_created: [
        {
            id: 'created',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} created issue {color|bold}{color|cyan}#{issue.id}{color|reset} ({color|orange}Priority: {issue.priority}{color|reset}, {color|lblue}Type: {issue.type}{color|reset}): {issue.title} - {shorten_link|issue.link}',
        },
    ],
    issue_updated: [
        {
            id: 'updated',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} updated issue {color|bold}{color|cyan}#{issue.id}{color|reset} {parse_changes|changes} - {shorten_link|issue.link}',
        },
    ],
    issue_comment_created: [
        {
            id: 'comment_created',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} created a comment on issue {color|bold}{color|cyan}#{issue.id}{color|reset} {issue.title} - {shorten_link|comment.link}',
        },
    ],

    /*
    * PULL REQUEST MESSAGES
    */
    pullrequest_created: [
        {
            id: 'created',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} created {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title} - {shorten_link|pr.link}',
        },
    ],
    pullrequest_updated: [
        {
            id: 'updated',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} updated {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title} - {shorten_link|pr.link}',
        },
    ],
    pullrequest_approved: [
        {
            id: 'approved',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} {color|green}approved{color|reset} {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title} - {shorten_link|pr.link}',
        },
    ],
    pullrequest_unapproved: [
        {
            id: 'unapproved_self',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} {color|red}removed{color|reset} their approval for {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title} - {shorten_link|pr.link}',
            condition: (event) => event.actor.username === event.approval.user.username,
        },
        {
            id: 'unapproved',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} removed {color|pink}{no_ping|approval.user.username}\'s{color|reset} approval for {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title} - {shorten_link|pr.link}',
        },
    ],
    pullrequest_fulfilled: [
        {
            id: 'merged',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} {color|green}merged{color|reset} {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title} - {shorten_link|pr.link}',
        },
    ],
    pullrequest_rejected: [
        {
            id: 'declined',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} {color|red}declined{color|reset} {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title} - {shorten_link|pr.link}',
        },
    ],
    pullrequest_comment_created: [
        {
            id: 'inline_comment_created',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} created a inline comment on {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title} ({comment.path}) - {shorten_link|comment.link}',
            condition: (event) => event.comment.is_inline,
        },
        {
            id: 'comment_created',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} created a comment on {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title} - {shorten_link|comment.link}',
        },
    ],
    pullrequest_comment_updated: [
        {
            id: 'inline_comment_updated',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} edited their inline comment on {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title} ({comment.path}) - {shorten_link|comment.link}',
            condition: (event) => event.comment.is_inline,
        },
        {
            id: 'comment_updated',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} edited their comment on {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title} - {shorten_link|comment.link}',
        },
    ],
    pullrequest_comment_deleted: [
        {
            id: 'inline_comment_deleted',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} deleted their inline comment on {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title} ({comment.path}) - {shorten_link|comment.link}',
            condition: (event) => event.comment.is_inline,
        },
        {
            id: 'comment_deleted',
            message: '[{color|blue}{repository.name}{color|reset}] {color|pink}{no_ping|actor.username}{color|reset} deleted their comment on {color|bold}{color|cyan}PR #{pr.id}{color|reset} {pr.title}'
        }
    ]
};
