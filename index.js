/* eslint-disable no-unused-vars */

const log = require('./logger')('index');
const packageJson = require('./package.json');
let startingText = '  Starting Bitbucket IRC Notifier v' + packageJson.version + ' by ' + packageJson.author + '  ';
log.info('='.repeat(startingText.length));
log.info(startingText);
log.info('='.repeat(startingText.length));

const irc = require('./irc');
const server = require('./server');

// Can't place this inside config.js because of recusiveness (config.js requires logger.js, which requires config.js, etc...)
const config = require('./config');
const configLog = require('./logger')('Config');
configLog.info('Loaded the config');
configLog.debug('Enviromental vars: ', process.env);
configLog.debug('Config: ', config);

let webserver = server();
