const _ = require('lodash');
const fs = require('fs');
const log = require('./logger')('Hooks');
const path = require('path');

let networks, hooks;

const refresh = () => {
    try {
        networks = JSON.parse(fs.readFileSync(path.join(__dirname, 'networks.json')));
        log.info('Loaded networks.json');

        let networkCopy = _.cloneDeep(networks);
        log.debug(Object.keys(networkCopy).map(key => { networkCopy[key].password = '***'; return networkCopy[key]; }));
    }
    catch (err) {
        log.error(`Error reading ${path.join(__dirname, 'networks.json')}!`);
        networks = {};
    }
    try {
        hooks = JSON.parse(fs.readFileSync(path.join(__dirname, 'hooks.json')));
        log.info('Loaded hooks.json!');
        log.debug(hooks);
    }
    catch (err) {
        log.error(`Rrror reading ${path.join(__dirname, 'hooks.json')}!`);
        hooks = {};
    }
};

const save = () => {
    try {
        fs.writeFileSync(path.join(__dirname, 'networks.json'), JSON.stringify(networks));
        log.info('Saving networks.json');
    }
    catch (err) {
        log.error(`Error writing to ${path.join(__dirname, 'networks.json')}!`);
    }
    try {
        fs.writeFileSync(path.join(__dirname, 'hooks.json'), JSON.stringify(hooks));
        log.info('Saving hooks.json');
    }
    catch (err) {
        log.error(`Error writing to ${path.join(__dirname, 'hooks.json')}!`);
    }
};

refresh();

module.exports = {
    hooks,
    hookList: Object.keys(hooks),
    networks,
    refresh,
    save,
};
