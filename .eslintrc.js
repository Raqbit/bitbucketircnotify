module.exports = {
    "extends": "standard",
    "parserOptions": { 
        "ecmaVersion": 6,
        "sourceType": "module"
    },
    "env": { "es6": true },
    "rules": {
        "semi": ["error", "always"],
        "indent": ["error", 4, {"SwitchCase": 1}],
        "comma-dangle": ["error", "only-multiline"],
        "brace-style": ["error", "stroustrup", {"allowSingleLine": true}]
    }
};