# Bitbucket Irc Notifier
The goal of this project is to provide a irc bot that can receive webhook requests from bitbucket for projects, and deliver them to irc channels for those projects. Aswell as making it realitively easy to run yourself on something like [heroku](http://heroku.com)

## Setup
0. Make sure you have git and new(ish) version of [node](https://nodejs.org) installed. 
1. clone the repo with `git clone https://bitbucket.org/PolarizedIons/bitbucketircnotify.git`
1. inside the directory (`cd bitbucketircnotify`), run `npm install` to install all the dependencies.
1. edit the config file (config.js), or set envorment variables for them (info below)
1. create a `networks.json` (from `networks.example.json`) and a `hooks.json` (from `hooks.example.json`) and edit it to your requirements. More on the format bellow.
1. setup webhooks (repo settings -> webhooks) to point to "http://`<your domain>`:`<your port>`/hook/`<your secret hook>`"
1. run the bot with `npm start`
1. Do something on the repo. The bot will join the network on the first event it gets

## Config
Each config option has a corrosponding enviroment variable the app prefers if set, as indicated in brackets.

 - `bind_port` (`PORT`) - (DEFAULT: 4810) The port it listens to for web requests
 - `colors` (`COLORS`) - (DEFAULT: true) Whether to use colors in irc messages

Additionally there is a `LOG_LEVEL` enviromental variable to override the log level used, and a `LOG_COLORS` to enable/disable log colors.

## Hooks(.json)
Each key is a hook (it should be secret and random) used to stop anyone/everyone from being able to spam irc with fake events.

Your hook url you use should match those defined in this file (eg. you have defined a hook "applesoranges" (but much more random), the url will be "http://`<your host>`:`<your port>`/hook/applesoranges").

Each hook should have the following values:

 - `network` - the name of the network you defined in `networks.json`
 - `channels` - the channels to join to annouce events on the hook, multiple channels must be comma seperated, with no spaces

## Networks(.json)
Each key is the name of a network that will be used in `hooks.json`. It can be anything you want, but advisably discriptive.

Each network should have the following values:

  - `host` - the host of the irc server which to connect to
  - `port` - the port of the irc server which to connect to
  - `ssl` - ['true' or 'false'], whether to use ssl for the connection or not
  - `user` - The nick/user to join & authenticate as
  - `password` - The password for the user to authenticate as via SASL

## Messages
Messages can be edited, added, removed, or even their conditions on when to display can be changed. They are all in `messages.js`. Please read the format of them in the comment at the top of the file.