const _ = require('lodash');
const config = require('./config');
const log = require('./logger')('Templater');

module.exports = templater;

function templater (string, data) {
    log.debug('Templating ', {string, data});
    let parserRegex = new RegExp(/{([^{]*?)\|([^}]*?)}/, 'g');
    let stringRegex = new RegExp(/{([^|{}]*?)}/, 'g');
    let result = string;

    let pMatch;
    while ((pMatch = parserRegex.exec(string)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (pMatch.index === parserRegex.lastIndex) {
            parserRegex.lastIndex++;
        }

        let parserName = pMatch[1];
        let parserKey = pMatch[2];
        let parserValue = parser(parserName, resolveKey(parserKey, data) || parserKey);
        log.debug(`[Parsing] Parser: ${parserName} (${parserKey} = ${parserValue})`);
        if (parserValue !== undefined) {
            result = result.replace(new RegExp('{' + parserName + '\\|' + parserKey + '}'), parserValue);
        }
    }

    let sMatch;
    while ((sMatch = stringRegex.exec(string)) !== null) {
        // This is necessary to avoid infinite loops with zero-width matches
        if (sMatch.index === stringRegex.lastIndex) {
            stringRegex.lastIndex++;
        }

        let stringKey = sMatch[1];
        let stringValue = resolveKey(stringKey, data);
        log.debug(`[Resolver] ${stringKey} = ${stringValue}`);
        if (stringValue !== undefined) {
            result = result.replace(new RegExp('{' + stringKey + '}'), stringValue);
        }
    }

    log.debug('Done: ', result);
    return result;
}

function resolveKey (key, data) {
    if (typeof key === 'string') {
        if (key.indexOf('.') === -1) {
            return key in data ? (typeof data[key] === 'string' ? data[key] : JSON.stringify(data[key])) : undefined;
        }

        key = key.split('.');
    }

    if (typeof data !== 'object') {
        return undefined;
    }

    if (key.length === 1) {
        return key in data ? (typeof data[key] === 'string' ? data[key] : JSON.stringify(data[key])) : undefined;
    }

    let top = key.splice(0, 1)[0];
    if (!(top in data)) {
        return undefined;
    }

    return resolveKey(key, data[top]);
}

function parser (parser, data) {
    const parsers = {
        parse_changes: (data) => {
            data = JSON.parse(data);
            let changes = {};
            for (let key of Object.keys(data)) {
                // Key in data contains two keys: 'old' and 'new'
                if (_.difference(Object.keys(data[key]), ['old', 'new']).length === 0) {
                    changes[key] = data[key];
                }
            }

            if (!Object.keys(changes).length) {
                return '';
            }

            let result = [];
            for (let key of Object.keys(changes)) {
                if (key === 'content') {
                    // Don't show changes in content
                    result.push('Changed content [not showing changes]');
                    continue;
                }

                let oldVal = changes[key].old.toString().replace(/_/g, ' ');
                let newVal = changes[key].new.toString().replace(/_/g, ' ');

                if (oldVal.length > 100 || newVal.length > 100) {
                    result.push(`${key.replace(/_/g, ' ')} [change too large to display]`);
                    continue;
                }

                result.push(`${key.replace(/_/g, ' ')}: '${oldVal}' -> '${newVal}'`);
            }
            return result.join(', ');
        },
        shorten_link: (link) => {
            let match;

            // Commit url
            if ((match = link.match(/https?:\/\/bitbucket\.org\/.*?\/.*?\/commits\/([0-9a-z]+)[/]?/) || '').length > 0) {
                // replace the full commit hash with a shorter(7 char) one
                return link.replace(match[1], match[1].substring(0, 7));
            }

            // Issue urls
            // https://bitbucket.org/PolarizedIons/bitbucketircnotify/issues/8/make-colours-available-to-use-in-messages
            else if ((match = link.match(/https?:\/\/bitbucket\.org\/.*?\/.*?\/issues\/[0-9]+\/(.+)[/]?/) || '').length > 0) {
                // remove the last portion of the url
                return link.replace(match[1], '').replace(/[/]*$/, '');
            }

            return link;
        },
        color: (color) => {
            if (!config.colors) {
                return '';
            }

            let ircColors = {
                'bold': '\x02',
                'italics': '\x1D',
                'underline': '\x1F',
                'reset': '\x0F',
                'white': '\x0300',
                'black': '\x0301',
                'blue': '\x0302',
                'green': '\x0303',
                'red': '\x0304',
                'brown': '\x0305',
                'magenta': '\x0306',
                'orange': '\x0307',
                'yellow': '\x0308',
                'lgreen': '\x0309',
                'cyan': '\x0310',
                'lcyan': '\x0311',
                'lblue': '\x0312',
                'pink': '\x0313',
                'grey': '\x0314',
                'gray': '\x0314',
                'lgrey': '\x0315',
                'lgray': '\x0315',
            };

            return ircColors[color];
        },
        no_ping: (string) => {
            let result = '';
            for (let segment of string.split(' ')) {
                result += segment.substring(0, 1) + '\u200B' + segment.substring(1); // Zero width space
            }
            return result;
        },
    };

    if (!(parser in parsers)) {
        return undefined;
    }
    return parsers[parser](data);
};
