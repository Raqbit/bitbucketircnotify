const _ = require('lodash');
const log = require('./logger')('MessageParser');
const messages = require('./messages');
const indexedMessages = indexMessages(messages);
const templater = require('./templater');

module.exports = messageParser;

function messageParser (eventKey, eventData) {
    log.debug('Parsing for event ' + eventKey);
    eventKey = eventKey.replace(':', '_');

    if (!(eventKey in messages) || !eventData || Object.keys(eventData).length === 0) {
        log.debug(`Failed: [eventKey in messages]: ${(eventKey in messages)}; [eventData]: ${!!eventData}; [eventData.length]: ${Object.keys(eventData).length}`);
        return [];
    }

    let potentialMessages = messages[eventKey];
    let chosenMessages;
    for (let potMsg of Object.keys(potentialMessages).map((key) => potentialMessages[key])) {
        let evaled;
        if ((evaled = evaluateMessage(potMsg, eventData)).length !== 0) {
            chosenMessages = evaled;
            break;
        }
    }

    if (!chosenMessages) {
        return [];
    }

    log.debug('Chose ' + chosenMessages.length + ' messages for ' + eventKey);
    chosenMessages.forEach(({message}) => {
        log.debug('  - ' + message.id);
    });

    return chosenMessages.map(({message, data}) => templater(message.message, data));
}

function indexMessages (messages) {
    let result = {};
    for (let eventKey of Object.keys(messages)) {
        for (let message of Object.keys(messages[eventKey]).map((key) => messages[eventKey][key])) {
            result[eventKey + '.' + message.id] = defaultMessage(message);
        }
    }
    return result;
}

function defaultMessage (messageObj) {
    return _.defaults(messageObj, {
        id: undefined,
        message: '',
        condition: () => true,
        run_on: (event) => [event],
        sub_messages: [],
    });
}

function evaluateMessage (messageObj, eventData) {
    let chosen = [];
    for (let runObj of messageObj.run_on(eventData)) {
        let runData = _.merge(runObj, eventData);
        if (messageObj.condition(runData)) {
            chosen.push({message: messageObj, data: runData});

            for (let subID of messageObj.sub_messages) {
                if (!(subID in indexedMessages)) {
                    continue;
                }

                evaluateMessage(indexedMessages[subID], _.merge(runData, {sub_message: true})).forEach((subMsg) => {
                    chosen.push(subMsg);
                });
            }
        }
    }
    return chosen;
}
