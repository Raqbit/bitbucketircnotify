const bodyParser = require('body-parser');
const config = require('./config');
const eventParser = require('./eventParser');
const express = require('express');
const hooks = require('./hooks');
const log = require('./logger')('Server');
const irc = require('./irc');
const messageParser = require('./messageParser');

module.exports = server;

function server () {
    const app = express()

        .use(bodyParser.json())

        .use((req, res, next) => {
            log.info(`${req.ip} ${req.method} ${req.originalUrl}`);
            next();
        })

        .post('/hook/:hook', (req, res) => {
            let eventKey = req.get('X-Event-Key');
            let hook = req.params.hook;
            if (eventKey && hooks.hookList.indexOf(hook) !== -1) {
                log.info('Valid hook POST made for ' + eventKey);
                log.debug('Request data: ', {hook, eventKey, payload: req.body});

                let event = eventParser(eventKey, req.body);
                log.debug('Parsed data: ', event);
                if (Object.keys(event).length) {
                    let messages = messageParser(eventKey, event);
                    if (messages) {
                        res.send('ok');
                        res.end();
                        irc.notify(hook, messages);
                        return;
                    }
                    else {
                        log.info('No messages matched for the provided event key (' + eventKey + ') and payload');
                    }
                }
            }
            log.info('Invalid hook POST made');
            log.debug({params: req.params, payload: req.body});
            res.send('no');
        })

        .all('*', (req, res) => {
            res.send('no');
        })

        .listen(config.bind_port, () => {
            log.info(`listening on port ${config.bind_port}`);
        });

    return app;
}
