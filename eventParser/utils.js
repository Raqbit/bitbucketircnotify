module.exports = {
    zeroPad,
    formatDate,
};

function zeroPad (string, paddedLength) {
    paddedLength = paddedLength || 2;
    string = string.toString();
    let amount = Math.max(paddedLength - string.length, 0);
    return '0'.repeat(amount) + string;
}

function formatDate (date, includeTime) {
    if (typeof date === 'string') {
        date = new Date(date);
    }
    if (includeTime === undefined) {
        includeTime = true;
    }

    let day = zeroPad(date.getDate());
    let month = zeroPad(date.getMonth() + 1);
    let year = zeroPad(date.getFullYear());

    let hours = zeroPad(date.getHours());
    let minutes = zeroPad(date.getMinutes());
    let seconds = zeroPad(date.getSeconds());

    return (includeTime ? `${hours}:${minutes}:${seconds} ` : '') + `${day}/${month}/${year}`;
}
