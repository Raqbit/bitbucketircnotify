const issue = require('./issue');
const log = require('../logger')('EventParser');
const pullRequest = require('./pull_request');
const repo = require('./repository');

module.exports = parser;

//
// Payload info here: https://confluence.atlassian.com/bitbucket/event-payloads-740262817.html
//

function parser (eventKey, payload) {
    try {
        log.debug('Parsing event ' + eventKey);
        switch (eventKey.split(':')[0]) {
            case 'repo':
                return repo(eventKey, payload);

            case 'issue':
                return issue(eventKey, payload);

            case 'pullrequest':
                return pullRequest(eventKey, payload);

            default:
                log.error('Unsupported event key ' + eventKey + ', skipping');
                return {};
        }
    }
    catch (err) {
        log.error('Error parsing:', err);
        return {};
    }
}
