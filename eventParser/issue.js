const {parseComment, parseIssue, parseRepository, parseOwner} = require('./entities');
const log = require('../logger')('EventParser/Issue');

module.exports = parse;

function parse (eventKey, payload) {
    switch (eventKey) {
        case 'issue:created':
            return created(payload);

        case 'issue:updated':
            return updated(payload);

        case 'issue:comment_created':
            return commentCreated(payload);

        default:
            log.error('Unsupported event ' + eventKey + ', skipping');
            return {};
    }
}
function created (payload) {
    return {
        actor: parseOwner(payload.actor),
        issue: parseIssue(payload.issue),
        repository: parseRepository(payload.repository),
    };
}

function updated (payload) {
    return {
        actor: parseOwner(payload.actor),
        issue: parseIssue(payload.issue),
        repository: parseRepository(payload.repository),
        comment: parseComment(payload.comment),
        changes: payload.changes
    };
}

function commentCreated (payload) {
    return {
        actor: parseOwner(payload.actor),
        repository: parseRepository(payload.repository),
        issue: parseIssue(payload.issue),
        comment: parseComment(payload.comment),
    };
}
