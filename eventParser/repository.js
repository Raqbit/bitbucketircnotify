const {parseComment, parseRepository, parseOwner} = require('./entities');
const {formatDate} = require('./utils');
const _ = require('lodash');
const log = require('../logger')('EventParser/Repo');

module.exports = parse;

function parse (eventKey, payload) {
    switch (eventKey) {
        case 'repo:push':
            return push(payload);

        case 'repo:fork':
            return fork(payload);

        case 'repo:updated':
            return updated(payload);

        case 'repo:transfer':
            return transfer(payload);

        case 'repo:commit_comment_created':
            return commitCommentCreated(payload);

        case 'repo:commit_status_created':
            return buildStatus(payload);

        case 'repo:commit_status_updated':
            return buildStatus(payload);

        default:
            log.error('Unsupported event ' + eventKey + ', skipping');
            return {};
    }
}

function push (payload) {
    return {
        actor: parseOwner(payload.actor),
        repository: parseRepository(payload.repository),
        changes: payload.push.changes.map((change) => ({
            branch_deleted: change.new == null,
            branch_created: change.old == null,
            branch: ((change.new || {}).type === 'branch' ? change.new.name : undefined) || ((change.old || {}).type === 'branch' ? change.old.name : undefined),
            tag: ((change.new || {}).type === 'tag' ? change.new.name : undefined) || ((change.old || {}).type === 'tag' ? change.old.name : undefined),
            diff: change.links.html.href,
            force_push: change.forced,
            commits: change.commits.map((commit) => ({
                hash: commit.hash,
                short_hash: commit.hash.substring(0, 7),
                message: commit.message.replace(/[\r\n]/g, ''),
                author: parseOwner(commit.author),
                link: commit.links.html.href,
            })),
            truncated: change.truncated,

            // Added for template ease
            last_commit: {
                hash: change.commits[0].hash,
                short_hash: change.commits[0].hash.substring(0, 7),
                message: change.commits[0].message.replace(/[\r\n]/g, ''),
                author: parseOwner(change.commits[0].author),
                link: change.commits[0].links.html.href,
            },
            commit_count: change.commits.length,
            from_hash: change.old.target.hash,
            from_short_hash: change.old.target.hash.substring(0, 7),
            from_hash_link: change.old.target.links.html.href,
            to_hash: change.new.target.hash,
            to_short_hash: change.new.target.hash.substring(0, 7),
            to_hash_link: change.new.target.links.html.href,
        })),
    };
}

function fork (payload) {
    return {
        actor: parseOwner(payload.actor),
        repository: parseRepository(payload.repository),
        fork: parseRepository(payload.fork),
    };
}

function updated (payload) {
    return {
        actor: parseOwner(payload.actor),
        repository: parseRepository(payload.repository),
        changes: payload.changes,
    };
}

function transfer (payload) {
    return {
        prev_owner: parseOwner(payload.previous_owner),
        new_owner: parseOwner(payload.actor),
        repository: parseRepository(payload.repository),
    };
}

function commitCommentCreated (payload) {
    return {
        actor: parseOwner(payload.actor),
        comment: parseComment(payload.comment),
        repository: parseComment(payload.comment),
        hash: payload.commit.hash,
        short_hash: payload.commit.hash.substring(0, 7),
    };
}

function buildStatus (payload) {
    return {
        actor: parseOwner(payload.actor),
        repository: parseRepository(payload.repository),
        status: _.merge(
            _.pick(payload.commit_status, [
                'name',
                'description',
                'state',
                'key',
                'type'
            ]),
            {
                link: payload.commit_status.url,
                raw_created: payload.created_on,
                created: formatDate(payload.created_on),
                raw_updated: payload.updated_on,
                updated: formatDate(payload.updated_on),
            }
        ),
    };
}
