const {parseComment, parsePullRequest, parseRepository, parseOwner} = require('./entities');
const {formatDate} = require('./utils');
const log = require('../logger')('EventParser/PR');

module.exports = parse;

function parse (eventKey, payload) {
    switch (eventKey) {
        case 'pullrequest:created':
            return created(payload);

        case 'pullrequest:updated':
            return updated(payload);

        case 'pullrequest:approved':
            return approved(payload);

        case 'pullrequest:unapproved':
            return unapproved(payload);

        case 'pullrequest:fulfilled':
            return merged(payload);

        case 'pullrequest:rejected':
            return declined(payload);

        case 'pullrequest:comment_created':
            return commentCreated(payload);

        case 'pullrequest:comment_updated':
            return commentUpdated(payload);

        case 'pullrequest:comment_deleted':
            return commentDeleted(payload);

        default:
            log.error('Unsupported event ' + eventKey + ', skipping');
            return {};
    }
}

function created (payload) {
    return {
        actor: parseOwner(payload.actor),
        pr: parsePullRequest(payload.pullrequest),
        repository: parseRepository(payload.repository),
    };
}

function updated (payload) {
    return {
        actor: parseOwner(payload.actor),
        pr: parsePullRequest(payload.pullrequest),
        repository: parseRepository(payload.repository),
    };
}

function approved (payload) {
    return {
        actor: parseOwner(payload.actor),
        pr: parsePullRequest(payload.pullrequest),
        repository: parseRepository(payload.repository),
        approval: {
            date: formatDate(payload.approval.date),
            user: parseOwner(payload.approval.user),
        }
    };
}

function unapproved (payload) {
    return {
        actor: parseOwner(payload.actor),
        pr: parsePullRequest(payload.pullrequest),
        repository: parseRepository(payload.repository),
        approval: {
            date: formatDate(payload.approval.date),
            user: parseOwner(payload.approval.user),
        }
    };
}

function merged (payload) {
    return {
        actor: parseOwner(payload.actor),
        pr: parsePullRequest(payload.pullrequest),
        repository: parseRepository(payload.repository),
    };
}

function declined (payload) {
    return {
        actor: parseOwner(payload.actor),
        pr: parsePullRequest(payload.pullrequest),
        repository: parseRepository(payload.repository),
    };
}

function commentCreated (payload) {
    return {
        actor: parseOwner(payload.actor),
        pr: parsePullRequest(payload.pullrequest),
        repository: parseRepository(payload.repository),
        comment: parseComment(payload.comment),
    };
}

function commentUpdated (payload) {
    return {
        actor: parseOwner(payload.actor),
        pr: parsePullRequest(payload.pullrequest),
        repository: parseRepository(payload.repository),
        comment: parseComment(payload.comment),
    };
}

function commentDeleted (payload) {
    return {
        actor: parseOwner(payload.actor),
        pr: parsePullRequest(payload.pullrequest),
        repository: parseRepository(payload.repository),
        comment: parseComment(payload.comment),
    };
}
