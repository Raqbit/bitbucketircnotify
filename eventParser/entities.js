const _ = require('lodash');
const {formatDate} = require('./utils');

module.exports = {
    parseComment,
    parseIssue,
    parseOwner,
    parseProject,
    parsePullRequest,
    parseRepository,
};

// 
// Parse common entities
// https://confluence.atlassian.com/bitbucket/event-payloads-740262817.html#EventPayloads-common_entitiesCommonentitiesforeventpayloads
// 

function parseOwner (payload) {
    if (payload.type === 'author') {
        // eslint-disable-next-line no-unused-vars
        let [match, username, email] = payload.raw.match(/(.*?) <(.*?)>/);
        return {
            username,
            email,
            display_name: username,
            link: '',
        };
    }
    else if (payload.type === 'user') {
        return _.merge(
            _.pick(payload, [
                'username',
                'display_name'
            ]),
            {
                link: payload.links.html.href,
                email: '',
            }
        );
    }

    return {};
}

function parseRepository (payload) {
    let parsed = _.merge(
        _.pick(payload, [
            'name',
            'full_name',
            'is_private',
            'website',
        ]),
        {
            link: payload.links.html.href,
            owner: payload.owner ? parseOwner(payload.owner) : undefined,
        }
    );

    // Optional fields
    if (payload.project) {
        parsed.project = parseProject(payload.project);
    }

    return parsed;
}

function parseProject (payload) {
    return _.merge(
        _.pick(payload, [
            'name',
            'key'
        ]),
        {
            link: payload.links.html.href,
        }
    );
}

function parseIssue (payload) {
    return _.merge(
        _.pick(payload, [
            'id',
            'component',
            'title',
            'priority',
            'state',
            'type',
        ]),
        {
            content: (payload.content.raw || '').substring(0, 100),
            full_content: payload.content.raw,
            milestone: (payload.milestone || {}).name,
            version: (payload.version || {}).name,
            raw_created: payload.created_on,
            created: formatDate(payload.created_on),
            raw_updated: payload.updated_on,
            updated: formatDate(payload.updated_on),
            link: payload.links.html.href,
        }
    );
}

function parseComment (payload) {
    return _.merge(
        _.pick(payload, [
            'id',
            'inline',
        ]),
        {
            content: (payload.content.raw || '').substring(0, 100),
            full_content: payload.content.raw,
            is_inline: !!payload.inline,
            created: formatDate(payload.created_on),
            raw_created: payload.created_on,
            updated: payload.updated_on ? formatDate(payload.updated_on) : undefined,
            raw_updated: payload.updated_on,
            link: payload.links.html.href,
        }
    );
}

function parsePullRequest (payload) {
    return _.merge(
        _.pick(payload, [
            'id',
            'title',
            'state',
            'author',
            'merge_commit',
            'closed_by',
            'reason',
        ]),
        {
            description: payload.description.substring(0, 100),
            full_description: payload.description,
            source: {
                branch: payload.source.branch.name,
                commit: payload.source.commit.hash,
                repository: parseRepository(payload.source.repository),
            },
            destination: {
                branch: payload.destination.branch.name,
                commit: payload.destination.commit.hash,
                repository: parseRepository(payload.destination.repository),
            },
            participants: payload.participants.map(parseOwner),
            reviewers: payload.reviewers.map(parseOwner),
            created: formatDate(payload.created_on),
            raw_created: payload.created_on,
            updated: formatDate(payload.updated_on),
            link: payload.links.html.href,
        }
    );
}
